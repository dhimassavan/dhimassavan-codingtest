import { View, Text } from 'react-native'
import React from 'react'
import Routing from './src/navigation/Routing'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import { SafeAreaView, StatusBar } from 'react-native'
import { persistor, store } from './src/utils/Reducers/store'

const App = () => {
  return (
    <Provider store={store}>
       <PersistGate loading={null} persistor={persistor}>
        <SafeAreaView style={{flex:1}}>
          <StatusBar barStyle={'dark-content'}/>
          <Routing/>
        </SafeAreaView>
     </PersistGate>
    </Provider>
  )
}

export default App