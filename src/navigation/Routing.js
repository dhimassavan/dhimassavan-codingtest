import {View, Text} from 'react-native';
import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native'



import Home from '../screen/Home/Home';
import Detail from '../screen/Home/Detail';


const Stack = createNativeStackNavigator();
export default function Routing() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={Home} options={{headerShown:false}}/>

        <Stack.Screen name="Detail1" component={Detail} options={{headerShown:false}}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}
