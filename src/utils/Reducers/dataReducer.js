const initialState = {
  contact: [],
};
const dataReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD_DATA': 
          return {
            ...state,
            contact: action.data,
          };
        case 'UPDATE_DATA':
          // ambil data contact sebelumnya
          var newData = [...state.contact];
          // mencari key , key = id
          var findIndex = state.contact.findIndex((value) => {
            return value.id === action.data.id;
          });
          //ganti data berdasarkan indexnya dengan data yg baru
          newData[findIndex] = action.data;
          return {
            ...state,
            contact: newData,
          };
          case 'DELETE_DATA':
            var newData=[...state.contact];
            // mencari key
            var findIndex = state.contact.findIndex((value)=> { return value.id === action.id})
          // hapus data berdasarkan index dengan splice 
          newData.splice(findIndex,1)
          return{
            ...state,
            contact: newData,
          }
    
        default:
          return state;
      } 
      
};

export default dataReducer;
