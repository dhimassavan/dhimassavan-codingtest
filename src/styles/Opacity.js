export const opacityColor = (colour, value) => {
    const opacity = Math.floor(0.1 * value * 255).toString(16);
    return colour + opacity;
}