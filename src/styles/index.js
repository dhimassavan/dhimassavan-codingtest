import * as Colors from './Color'
import * as opacityColor from './Opacity'

export { Colors, opacityColor }