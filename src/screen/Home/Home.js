import {
  View,
  FlatList,
  TouchableOpacity,
  Image,
  ActivityIndicator,
  Modal,
  RefreshControl,
  ScrollView,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {BASE_URL, TOKEN} from '../../utils/service/url';
import Icon from 'react-native-vector-icons/AntDesign';
import {useIsFocused} from '@react-navigation/native';
import Axios from 'axios';
import API from '../../utils/service/apiProvider';
import { Text } from 'react-native';
import HomeComponent from '../../component/section/Home/HomeComponent';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import { useDispatch, useSelector } from 'react-redux';


const Home = ({navigation}) => {
  const [dataContact, setDataContact] = useState({});
  const [showModalAdd, setShowModalAdd] = useState(false);
  const [refreshing, setRefreshing] = React.useState(false);
  const isFocused = useIsFocused();
  const {contact} = useSelector(state => state.data)
  const dispatch = useDispatch();
  const onPost = async (params)=> {
    const response = await API.postData(params);
    alert('berhasil di tambahakan')
    getDataContact()
    navigation.navigate('Home')
  }
  
  useEffect(() => {
    getDataContact();
  }, [isFocused]);
  
  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    getDataContact();
    setTimeout(() => {
      setRefreshing(false);
    }, 1000);
  }, []);

  const getDataContact = async () => {
    setShowModalAdd(false);
    const response = await API.getData();
    if(response){
      setDataContact(response);
      dispatch({ type: 'ADD_DATA', data: response.data });
    }
    setTimeout(() => {
      setShowModalAdd(true);
    }, 1000);
  };
  

  

  return (
    <View style={{flex:1 , backgroundColor: Colors.White}}>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
          <HomeComponent
          dataContact={dataContact}
          onPost={onPost}
          navigation={navigation}
          
          />
           
      </ScrollView>
    </View>
  );
};

export default Home;
