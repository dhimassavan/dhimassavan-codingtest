import { View, Text } from 'react-native'
import React from 'react'
import DetailComponent from '../../component/section/Home/DetailComponent'
import apiProvider from '../../utils/service/apiProvider';

const Detail = ({navigation,route}) => {
    const dataDetail = route.params;
    const deleteData = async () => {
        const id = dataDetail.id
        
        const response = await apiProvider.deleteData(id);
        
        navigation.navigate('Home')
        alert('berhasil di hapus')
        }

    const onEdit = async (params)=> {
        
        const id = dataDetail.id
        const response = await apiProvider.editData(id,params);
      if(response?.status == 201 || response?.status == 200){
        navigation.navigate('Home')
        alert('berhasil di edit')
      }
      else{
        alert('gagal')
    
      }
      
      }
   
      
      
    
  return (
    <View style={{flex:1}}> 
      <DetailComponent 
      dataDetail={dataDetail}
      navigation={navigation}
      onEdit={onEdit}
      onDelete={deleteData}
      />
    </View>
  )
}

export default Detail