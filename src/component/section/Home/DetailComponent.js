import {View, Text} from 'react-native';
import React, {useEffect, useState} from 'react';
import {InputText, TextBold, TextMedium, TextRegular} from '../../global';
import {TouchableOpacity} from 'react-native';
import {Colors} from '../../../styles';
import Icon from 'react-native-vector-icons/AntDesign';
import {Image} from 'react-native';
import Icon1 from 'react-native-vector-icons/MaterialIcons';
import ModalCenter from '../../modal/ModalAddData';
import {StyleSheet} from 'react-native';

const DetailComponent = ({navigation, dataDetail, onEdit, onDelete}) => {
  const [showModalAdd, setShowModalAdd] = useState(false);
  const [namaDepan, setNamaDepan] = useState('');
  const [namaBelakang, setNamaBelakang] = useState('');
  const [photo, setPhoto] = useState('');
  const [umur, setUmur] = useState('');
  useEffect(() => {
    if (dataDetail) {
      setNamaBelakang(dataDetail.lastName);
      setNamaDepan(dataDetail.firstName);
      setUmur(dataDetail.age);
      setPhoto(dataDetail.photo);
    }
  }, []);

  const handelonEdit = () => {
    const params = {
      firstName: namaDepan,
      lastName: namaBelakang,
      age: umur,
      photo: photo || 'N/A',
    };
    onEdit(params);
  };
  return (
    <View style={{}}>
      <ModalCenter
        show={showModalAdd}
        onClose={() => setShowModalAdd(false)}
        title="EDIT Contact">
        <View>
          <View style={styles.viewModal}>
            <InputText
              style={styles.textinput}
              placeholderText="Nama Depan "
              value={namaDepan}
              onChangeText={text => setNamaDepan(text)}
            />
            <InputText
              style={styles.textinput}
              placeholderText="Nama Belakang "
              value={namaBelakang}
              onChangeText={text => setNamaBelakang(text)}
            />
            <InputText
              style={styles.textinput}
              placeholderText="Umur"
              value={umur}
              onChangeText={text => setUmur(text)}
            />
            <InputText
              style={styles.textinput}
              placeholderText="Link Gambar"
              value={photo}
              onChangeText={text => setPhoto(text)}
            />
            <TouchableOpacity
              onPress={() => {
                handelonEdit();
                setShowModalAdd(false);
              }}
              style={styles.tombolSimpan}>
              <TextBold text="SIMPAN" color={Colors.WHITE} />
            </TouchableOpacity>
          </View>
        </View>
      </ModalCenter>
      <View style={styles.header}>
        <TouchableOpacity
          style={styles.tombolBack}
          onPress={() => navigation.navigate('Home')}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Icon name="leftsquare" size={18} color={Colors.BLACK} />
            <TextBold text="BACK" color={Colors.BLACK} />
          </View>
        </TouchableOpacity>
        <TextBold
          style={{
            color: 'white',
          }}
          text="Detail Contact"
          size={20}></TextBold>
      </View>
      <View style={styles.content}>
        <Image
          style={styles.imageContent}
          source={
            dataDetail.photo === 'N/A'
              ? require('./../../../assets/icons/user.png')
              : {uri: dataDetail.photo}
          }
        />
        <View style={{marginTop: 10}}>
          <TextBold
            text={dataDetail.firstName + ' ' + dataDetail.lastName}
            size={20}
            color={Colors.BLACK}
          />
        </View>
        <View
          style={styles.optionCall}>
          <TouchableOpacity>
            <Icon1 name="call" size={45} color={Colors.BLACK} />
          </TouchableOpacity>
          <TouchableOpacity>
            <Icon1 name="video-call" size={60} color={Colors.BLACK} />
          </TouchableOpacity>
          <TouchableOpacity>
            <Icon1 name="chat" size={45} color={Colors.BLACK} />
          </TouchableOpacity>
        </View>

        <View
          style={styles.details}>
          <TextBold text="Main Details :" size={20} color={Colors.BLACK} />
          <View
            style={{
              width: '100%',
              borderBottomWidth: 1,
              paddingVertical: 15,
            }}>
            <TextRegular
              color={Colors.BLACK}
              text={
                'Name : ' + dataDetail.firstName + ' ' + dataDetail.lastName
              }
              size={22}
            />
          </View>
          <View
            style={{
              width: '100%',
              borderBottomWidth: 1,
              paddingVertical: 15,
            }}>
            <TextRegular
              color={Colors.BLACK}
              text={'Age : ' + dataDetail.age + ' years'}
              size={22}
              style={{fontWeight: '500'}}
            />
          </View>

          <TouchableOpacity
            style={styles.tombol}
            onPress={() => setShowModalAdd(true)}>
            <TextBold text="Edit" size={20} color={Colors.WHITE} />
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              borderRadius: 15,
              borderWidth: 1,
              paddingVertical: 10,
              width: '100%',
              alignItems: 'center',
              backgroundColor: Colors.BLACK,
            }}
            onPress={() => onDelete()}>
            <TextBold text="Delete" size={20} color={Colors.WHITE} />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  viewModal: {
    backgroundColor: Colors.BLACK,
    paddingHorizontal: 10,
  },
  textinput: {
    marginTop: 10,
    backgroundColor: Colors.WHITE,
  },
  tombolSimpan: {
    alignItems: 'center',
    marginVertical: 10,
    paddingVertical: 15,
    paddingHorizontal: 10,
    backgroundColor: Colors.DEEPORANGE,
  },
  header: {
    paddingVertical: 15,
    paddingHorizontal: 20,
    backgroundColor: 'black',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  tombolBack: {
    padding: 7,
    borderRadius: 10,
    backgroundColor: Colors.WHITE,
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    width: '100%',
    alignItems: 'center',
    paddingVertical: 20,
    backgroundColor: Colors.GRAY,
  },
  imageContent:{
    height: 170,
    width: 170,
    borderRadius: 85,
    marginTop: 10,
  },
  optionCall:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 10,
  },
  details:{
    width: '100%',
    paddingHorizontal: 30,
    marginTop: 20,
    backgroundColor: Colors.WHITE,
    height: '100%',
    paddingVertical: 10,
    borderRadius: 20,
  },
  tombol:{
    marginVertical: 20,
    borderRadius: 15,
    borderWidth: 1,
    paddingVertical: 10,
    width: '100%',
    alignItems: 'center',
    backgroundColor: Colors.BLACK,
  }
});
export default DetailComponent;
