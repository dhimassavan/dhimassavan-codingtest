import {View, Text, StyleSheet} from 'react-native';
import {convertCurrency} from '../../utils/helpers';
import {InputText, TextBold, TextMedium, TextRegular} from '../../global/index';
import Icon from 'react-native-vector-icons/AntDesign';
import React, {useEffect, useState} from 'react';
import ModalCenter from '../../modal/ModalAddData';
import {Colors} from '../../../styles';
import {FlatList} from 'react-native';
import {TouchableOpacity} from 'react-native';
import {Image} from 'react-native';
import {useSelector} from 'react-redux';

const HomeComponent = ({navigation, dataContact, onPost}) => {
  const [showModalAdd, setShowModalAdd] = useState(false);
  const [namaDepan, setNamaDepan] = useState('');
  const [namaBelakang, setNamaBelakang] = useState('');
  const [umur, setUmur] = useState('');
  const [photo, setPhoto] = useState('');
  const [search, setSearch] = useState('');
  const {contact} = useSelector(state => state.data);
  const handelonPost = () => {
    const params = {
      firstName: namaDepan,
      lastName: namaBelakang,
      age: umur,
      photo: photo || 'N/A',
    };
    onPost(params);
    setNamaDepan(''); 
    setNamaBelakang(''); 
    setUmur(''); 
    setPhoto('');
  };
  return (
    <View style={{flex: 1}}>
      <ModalCenter
        show={showModalAdd}
        onClose={() => setShowModalAdd(false)}
        title="Add Contact">
        <View>
          <View style={styles.viewModal}>
            <InputText
              style={styles.textinput}
              placeholderText="Nama Depan "
              value={namaDepan}
              onChangeText={text => setNamaDepan(text)}
            />
            <InputText
              style={styles.textinput}
              placeholderText="Nama Belakang "
              value={namaBelakang}
              onChangeText={text => setNamaBelakang(text)}
            />
            <InputText
              style={styles.textinput}
              placeholderText="Umur"
              value={umur}
              onChangeText={text => setUmur(text)}
            />
            <InputText
              style={styles.textinput}
              placeholderText="Link Gambar"
              value={photo}
              onChangeText={text => setPhoto(text)}
            />
            <TouchableOpacity
              onPress={() => {
                handelonPost();

                setShowModalAdd(false);
              }}
              style={styles.tombolSimpan}>
              <TextBold text="SIMPAN" size={16} color={Colors.WHITE} />
            </TouchableOpacity>
          </View>
        </View>
      </ModalCenter>
      <View
        style={{
          paddingVertical: 15,
          paddingHorizontal: 20,
          backgroundColor: 'black',
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <TextBold
            style={{
              color: Colors.WHITE,
            }}
            text="My Contact"
            size={20}></TextBold>

          <TouchableOpacity
            style={styles.tombolAdd}
            onPress={() => setShowModalAdd(true)}>
            <TextBold text="Tambah Data" color={Colors.BLACK} />
          </TouchableOpacity>
        </View>
        <InputText
          onChangeText={text => setSearch(text)}
          style={styles.textinput}
          placeholderText="Search"
        />
      </View>
      <View style={{}}>
        <FlatList
          data={contact}
          style={{height: '100%'}}
          contentContainerStyle={{paddingBottom: 10}}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item, index}) => (
            <TouchableOpacity
              onPress={() => navigation.navigate('Detail1', item)}
              activeOpacity={0.8}
              style={styles.flatlistView}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  borderBottomWidth: 1,
                }}>
                <View
                  style={{
                    padding: 10,
                  }}>
                  <Image
                    style={styles.imageFlatlist}
                    source={
                      item.photo === 'N/A'
                        ? require('./../../../assets/icons/user.png')
                        : {uri: item.photo}
                    }
                  />
                </View>
                <View style={{padding: 10}}>
                  <TextMedium
                    text={'Nama : ' + item.firstName + ' ' + item.lastName}
                    color={Colors.BLACK}
                    size={16}></TextMedium>
                </View>
              </View>
            </TouchableOpacity>
          )}
        />
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  viewModal: {
    backgroundColor: Colors.BLACK,
    paddingHorizontal: 10,
  },
  textinput: {
    marginTop: 10,
    backgroundColor: Colors.WHITE,
  },
  tombolSimpan: {
    alignItems: 'center',
    marginVertical: 10,
    paddingVertical: 15,
    paddingHorizontal: 10,
    backgroundColor: Colors.DEEPORANGE,
  },
  tombolAdd: {
    padding: 10,
    borderRadius: 10,
    backgroundColor: Colors.WHITE,
    justifyContent: 'center',
    alignItems: 'center',
  },
  flatlistView: {
    width: '100%',
    alignSelf: 'center',
    borderColor: 'black',
    paddingHorizontal: 20,
    backgroundColor: 'white',
    justifyContent: 'flex-start',
  },
  imageFlatlist: {
    width: 60,
    height: 60,
    borderRadius: 30,
    resizeMode: 'cover',
  },
});
export default HomeComponent;
